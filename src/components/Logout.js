import React from 'react'
import { useSelector,useDispatch } from 'react-redux';
import { logout, selectUser } from '../features/userSlice';
import './Logout.css';

const Logout = () => {
  const user = useSelector(selectUser)
  
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("logout")

    
    dispatch(logout())
  }
  return (
    <div className='logout'>
        <h1>
            Welcome <span className='user_name'>{user.name}</span>
        </h1>
          <button className='logout_button' onClick={(e) => handleSubmit(e)}>Logout</button>
    </div>
  )
}

export default Logout